# Creating a shape binder

---
<i class="fa fa-info-circle" style="color:#4d45c2"><!-- icon --></i>

A shape binder works as a connecting template between different bodies (or parts). It provides a guide, and can be used as reference to attach sketches and to apply constraints.

---

## Shape binder

Switch to the Part Design workbench and create a **Part** and a **Body**. The body should be active after creating it, but you can activate by double-clicking on it.

![Create a part and a body](images/03_binder_part_body.png)

Then, rotate the view to see the bottom of the PCB, click to select the bottom face and create a shape binder from it.

![Rotate the view](images/03_binder_select_pcb_bottom.png)

The combo view will display the shape binder options (_datum shape parameters_), a face should appear on the list. Click OK.

![Shape binder parameters](images/03_binder_shape_binder_param.png)

Now the shape binder should appear on the design view. On this image, the PCB model **visibility** has been set to **_false_** for better view. You can do that by selecting an element from the combo view and pressing _'space bar'_.

![Shape binder created](images/03_binder_shape_binder_view.png)

Make sure the shape binder has been created inside the body. If not, you can **right_click** on it and move it to another body.

![Shape binder-body relation](images/03_binder_shape_binder_body_tree.png)

_Make sure you save your FreeCAD project._
