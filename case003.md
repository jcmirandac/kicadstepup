# Designing the top cover

---
<i class="fa fa-info-circle" style="color:#4d45c2"><!-- icon --></i>

Following a similar process you will design the top cover using a shape binder.

---

## New Part

Because the top cover is a separate part of the model, you need to create a new **Part**, and inside it a new **Body**.

![Create a new part and body](images/05_cover_01_new_part.png)

Select the top face of the box walls and create a shape binder ![](images/05_cover_03_shape_binder_tool.png).

![Create a new shape binder](images/05_cover_02_shape_binder.png)

Verify the shape binder was created inside the new body and part.

![Combo view](images/05_cover_04_combo_view.png)

Select the shape binder and create a new sketch on it. You may need to hide the box to select the shape binder.

![Create a skecth on the new shape binder](images/05_cover_05_hide_box_walls.png)

Use the edges of the shape binder to create a rectangle the same size as the inner shape.

![Drawing on external geometry edges](images/05_cover_06_first_cover_sketch.png)

Then create a pad ![](images/04_sketch_09_pad_tool.png) from it. On the pad parameters select _reversed_ and set the _length_ to 1 mm.

![Create a pad from the sketch](images/05_cover_07_first_cover_pad.png)

If the shape binder disappears, go to the combo view and unhide it. Once again, select the shape binder and create a new sketch on it. This time, create a rectangle the same size as the outer shape.

![New sketch on the same shape binder](images/05_cover_08_second_cover_sketch.png)

Now create another pad. This pad should be 2 mm. The cover now should look like the picture:

![Pad the new sketch](images/05_cover_09_second_cover_pad.png)

This is final model of the case, based on a PCB model and using shape binders to ensure the parts fit each other. Ypu can apply transparency to some bodies in order to have a clear view of the inner components.

![Finished case](images/05_cover_10_finished_case.png)

You can apply the same process to create any case based on a KiCAD PCB model.

_Make sure you save your FreeCAD project._
