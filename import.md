# Importing a KiCAD pcb file

---
<i class="fa fa-info-circle" style="color:#4d45c2"><!-- icon --></i>

Once StepUp is installed and the pcb model is finished, you are ready to import it into FreeCAD.

---

##KiCadStepUp workbench

Create a new FreeCAD file and go to KiCadStepUp workbench

![Select StepUp workbench](images/02_import_freecad_new_file.png)

Click the button to import KiCAD PCB

![PCB import button](images/02_import_freecad_import_pcb.png)


View of the PCB imported
![PCB imported into FreeCAD](images/02_import_freecad_imported_pcb.png)

_Now you can start designing your case._
