# Summary

* [1. Prepare KiCAD file](prepare.md)
* [2. Import PCB model](import.md)
* [3. Shape binder](case001.md)
* [4. Base box](case002.md)
* [5. Top cover](case003.md)
* [6. Adding screws](extra001.md)

