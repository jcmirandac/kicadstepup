# Add screw holes to the case

---
<i class="fa fa-info-circle" style="color:#4d45c2"><!-- icon --></i>

One way to secure the top cover to the case is using holes for screws.

---




First, hide the _Part_ that contains the bottom part of the case (the box). Also make sure you have the top cover _Body_ selected.

![Top cover bottom view](images/06_screws_02_bottom_top_cover.png)

Before creating the next sketch, it is a good idea to also hide the PCB model _Part_ because you will need to work on the bottom face of the cover. Then click on the bottom face of the cover and create a new sketch ![](images/06_screws_01_sketch_tool.png).

![Select top cover bottom face](images/06_screws_03_bottom_cover_sketch.png)

Select the vertical borders as external geometries. Then do the following:

* add a circle
* select the lower left vertex and the circle center vertex
* apply lock constraint to the vertexes
* set the horizontal distance to 2,3 mm
* set the vertical distance to 35 mm
* select the circle and add a radius constraint set to 0,6 mm
* add anothe circle on the right side of the case cover

Radius constraint tool: ![](images/06_screws_04_circle_constrain_tool.png)

![Create sketch for screws holes](images/06_screws_05_screw_holes_sketch.png)

### Holes on the case bottom

Now, you need to create another shape binder. Make sure the first body is selected (even if it is still hidden), then select the top cover bottom face and create the shape binder. Double check the shape binder is created in the body taht contains the case bottom.

![Create sketch for screws holes](images/06_screws_06_cover_bottom_shape_binder.png)

Now, if you hide the top cover and unhide the case botton (and PCB model), you can see that the shape binder is in the place where the bottom of the top cover would be, including the new screw holes.

![Create sketch for screws holes](images/06_screws_07_new_shape_binder.png)

Once again, hide the case bottom and the PCB model, and create a new sketch **on the bottom side** of the shape binder.

_**Note:** to hide the case bottom, do not hide the Body, instead, hide the pocket that is before the shape binder in the combo view._

![Create a new shape binder](images/06_screws_08_bottom_new_shape_binder.png)

Using the external geometry tool select the two vertical edges of the recatngle and the circles of the screw holes.

![Create a new shape binder](images/06_screws_09_sketch_bottom_new_shape_binder.png)

Draw a circle using the same center of the circle in the shape binder. Then, to make the circle the same size as the external geometry guide, select the two circumferences and apply the **equality constraint**.

![Create a new shape binder](images/06_screws_10_sketch_bottom_equal_constraint.png)

After that, draw a rectangle around the circle. Use the vertical external geometry guide to constrain one side of the rectangle. Then apply **lock constraint** and **distance constraint** to the other vertexes of the rectangle. Do the same on the other side of the shape binder.

![Create a new shape binder](images/06_screws_11_sketch_bottom_screw_bases.png)

Close the sketch and create a 10 mm pad from it. You should now have a base on each side for screwing the top cover to the bottom case. Unhide the other parts to se the case finished.

![Create a new shape binder](images/06_screws_12_pad_screw_bases.png)

_Make sure you save your FreeCAD project._
