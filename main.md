# KiCAD StepUp

---
<i class="fa fa-info-circle" style="color:#4d45c2"><!-- icon --></i>

How to design a case in FreeCAD around a pcb model from KiCAD.

KiCAD <> FreeCAD integration. KicadStepUp is an additional workbench that you can add to FreeCAD. It enables FreeCAD to import 3D pcb models from KiCAD.

---

##Installing KiCAD StepUp

You can find more information here: [https://github.com/easyw/kicadStepUpMod/](https://github.com/easyw/kicadStepUpMod/).

* Open FreeCAD
* Go to Tools >> addon_manager
* Look for kicadStepUpMod (mine is already installed)

![addon menu](images/00_intro_addon_manager.png)
* Click on install/update
* If it was correctly installed it should now appear in the workbenches list

![workbenches menu](images/00_intro_workbench_menu.png)
