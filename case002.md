# Designing the case

---
<i class="fa fa-info-circle" style="color:#4d45c2"><!-- icon --></i>

With the PCB model imported and the shape binder set, you can start designing your case around the PCB.

---

## Sketching

You should still be in the Part Design workbench. Select the shape binder and create a new sktech based on it.

![Create a skecth on a shape binder](images/04_sketch_01_binder_sketch.png)

Select the tool to **create an edge linked to an external geometry**. This will create lines and vertexes of reference for the current sketch so that the user can apply constraints.

![Create edge on external geometry](images/04_sketch_02_external_geometry.png)

Then select the top and bottom edgest of the shape binder. When the edge is created, a line and vertexes will appear in the sketch. These are guides to draw on or to apply constraints.

![External geometry edges](images/04_sketch_03_external_geometry.edges.png)

Create a rectangle bigger thant the shape binder (the shape binder represents the bottom of the PCB).

![Drawing around external geometry edges](images/04_sketch_04_external_geometry_draw.png)

Now apply constraints to the rectangle. We will use the ![](images/04_sketch_05_lock_constraint_button.png) **lock constraint** for that.

Select the upper right corner vertexes of the external edge and the created rectangle. Then click on the **lock constraint** tool.

![Apply lock constraint](images/04_sketch_06_apply_lock_constraint.png)

Two dimensions will appear. Double click each one and change them to 6 mm.

![Apply lock constraint](images/04_sketch_07_dimension_lock_constraint.png)

Do the same with the lower left corner vertexes. Then add another rectangle and repeat the process but this time at 4 mm. Close the sketch.

![Another rectangle at 4 mm](images/04_sketch_08_second_rectangle.png)

Convert the sketch to a pad with the pad tool ![](images/04_sketch_09_pad_tool.png) and apply **Two dimensions type**:

length: 60 mm

second legth: 20 mm

A wall will be created around the PCB.

![Create a pad for the walls](images/04_sketch_10_pad_param.png)

Select again the shape binder and create a new sketch. Use the external geometry tool to select the inner edges of the walls you just created. Create a rectangle using those edges. Then make a smaller rectangle and use lock constraints to separate the two rectangles by 6 mm.

![Create a sketch for the support](images/04_sketch_11_support_pcb.png)

Close the sketch and create a pad of 6 mm thickness.

![Create a pad for the support](images/04_sketch_12_support_pcb_pad.png)

Select the bottom face of the outer rectangle and create a new sketch. Use external geometry tool to select the outer edges and create a rectangle using thos exact vertexes. Close the sketch and create a 2 mm pad. This is now the bottom of the case.

![Create a pad for the bottom](images/04_sketch_13_bottom_case.png)

Finally, the case needs an opening in front of the usb port so that it can be accessed from the outside. Hide the case and close up on the usb port.

![View of the usb connector](images/04_sketch_14_usb_view.png)

Now the same process is applied. Select the front face of the usb and create a shape binder. Hide the PCB model. Select the new shape binder and create a skecth. Create a rectangle around the usb outer edge, separated by 1 mm. Close the sketch.

![Create sketch for the usb conector hole](images/04_sketch_15_usb_hole.png)

Unhide the case and convert the sketch to a hole with the pocket tool ![](images/04_sketch_16_pocket_tool.png) and apply reversed 6 mm dimension. Make sure the pocket goes through the wall of the case.

![Create a pocket for the usb connector](images/04_sketch_17_pocket_wall.png)

Unhide the PCB model so you can see the lower part of the case finished.

![Lower part of the case finished](images/04_sketch_18_final_bottom_case.png)

_Make sure you save your FreeCAD project._
