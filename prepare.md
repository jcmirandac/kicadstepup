# Preparing the KiCAD file

---
<i class="fa fa-info-circle" style="color:#4d45c2"><!-- icon --></i>

A few details must be set before importing the KiCAD file into FreeCAD.

The KiCAD file shown in this tutorial can be dowloaded <a target="_blank" href="files/atmega32u2_basic.zip">here</a>.

---

To take full advantage of this tool, first make sure that your KiCAD model is correctly set up:

### Define the PCB edge
This is a closed shape on the layer 'Edge.Cuts'. It can have any shape and it can also have mounting holes if needed:
![Edges defined](images/01_prepare_edges_gold.png)

### Define auxiliary axis and grid origin:

* Auxiliary axis must be placed near the lower left corner of the pcb.
* Origin point for the grid can be placed on the geometric center of the pcb or on other areas (corners, etc.).

![Axis and origin defined](images/01_prepare_origins_kicad_001.png)

### Assign 3D models to all components

It is also recommended to assign 3D models to all the components on the board (at least the bigger ones). The components that do not have a model assigned, won't show in FreeCAD.

To do this:

* on the PCB editor select a footprint and press 'E' (edit properties) and go to '3D Settings':

![footprint properties](images/01_prepare_footprint_prop_001.png)

* in '3D Settings' click on the folder to add a 3D model from the library:

![3d settings](images/01_prepare_footprint_prop_002.png)

* Then go through the folder to find the right 3D model: pakcages3d >> component category >> specific model

![select 3d model](images/01_prepare_select_3dmodel_all.png)

* When you find the right model, check that it has the correct orientation and placement, you can modify these parameters on the left panel:

![modify 3d model](images/01_prepare_footprint_prop_003.png)

* Finally, you can go to **View >> 3D Viewer** to check the final model:
![view 3d model](images/01_prepare_3dviewer_kicad.png)

_Make sure you save your KiCAD project._
